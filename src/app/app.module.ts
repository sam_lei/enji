import {NgModule, ErrorHandler, LOCALE_ID} from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { NearPage } from '../pages/near/near';
import { EventsPage } from '../pages/events/events';
import { TabsPage } from '../pages/tabs/tabs';
import {EventDetailPage} from "../pages/event-detail/event-detail";
import {ApiService} from "./services/api";
import {EventCreatePage} from "../pages/event-create/event-create";
import {NearChatPage} from "../pages/near-chat/near-chat";
import {Storage} from '@ionic/storage';
import {HomePage} from "../pages/home/home";
import {PlaceService} from "./services/places";
import {JsonpModule} from '@angular/http';
import {GeoService} from "./services/geo";

@NgModule({
  declarations: [
    MyApp,
    EventsPage,
    EventDetailPage,
    EventCreatePage,
    NearPage,
    NearChatPage,
    HomePage,
    TabsPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    JsonpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    EventsPage,
    EventDetailPage,
    EventCreatePage,
    NearPage,
    NearChatPage,
    HomePage,
    TabsPage
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    { provide: LOCALE_ID, useValue: "de" },
    Storage,
    ApiService,
    GeoService,
    PlaceService
  ]
})
export class AppModule {}
