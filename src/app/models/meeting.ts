import {Message} from "./message";

export interface Meeting {
  event_id: string;
  user_id: string;
  title: string;
  timestamp: any;
  location_description: string;
  latitude: number;
  longitude: number;
  description: string;
  max_members: number;
  current_members: number;
  chat?: Message[];
}

export class NewMeeting implements Meeting {
  event_id = undefined;
  user_id = undefined;
  title = "";
  timestamp = new Date().toISOString();
  location_description = "";
  latitude = undefined;
  longitude = undefined;
  description = "";
  max_members = 5;
  current_members = 0;
  chat = [];
}
