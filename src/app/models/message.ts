export interface Message {
  sender_id: string;
  receiver_id: any;
  timestamp: number;
  text: string;
}

export class NewMessage implements Message {
  sender_id;
  receiver_id;
  timestamp;
  text;

  constructor(sender: string = "", receiver: any = "", text: string = "") {
    this.sender_id = sender;
    this.receiver_id = receiver;
    this.timestamp = 0;
    this.text = text;
  }
}
