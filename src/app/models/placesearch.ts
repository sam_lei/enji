export interface Placesearch {
  key: string;
  location: string; // long,lat
  radius: number; // max 50.000m
  keyword: string;
  language: string;
}


export class NewPlacesearch implements Placesearch {
  key;
  location; // long,lat
  radius; // max 50.000m
  keyword;
  language;

  constructor(lat: number, long: number, keyword: string) {
    this.key = "AIzaSyDpt8yySoQgGZwT4AlTAa1vbqltrSG0CL4";
    this.location = long + "," + lat;
    this.radius = 5000;
    this.keyword = keyword;
    this.language = "de";
  }
}
