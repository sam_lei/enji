export interface User {
  user_id: string;
  status: string;
  time: number;
  distance: number; // in km
  latitude: number;
  longitude: number;
}


export class NewUser implements User {
  user_id;
  status;
  time;
  distance;
  latitude;
  longitude;

  constructor() {
    this.user_id = "unset";
    this.status = "unset";
    this.time = 0;
    this.distance = 0;
    this.latitude = 0;
    this.longitude = 0;
  }
}
