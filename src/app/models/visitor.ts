export interface Visitor {
  event_id: string;
  user_id: string;
}


export class NewVisitor implements Visitor {
  event_id;
  user_id;

  constructor(event_id: string, user_id: string) {
    this.event_id = event_id;
    this.user_id = user_id;
  }
}
