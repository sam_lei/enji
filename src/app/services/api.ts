import {Injectable} from '@angular/core';
import {Device} from "ionic-native";
import {Message} from "../models/message";
import {Meeting} from "../models/meeting";
import {Observable, Subject} from "rxjs";
import {Headers, Http, Response} from "@angular/http";
import {User, NewUser} from "../models/users";
import {Platform} from "ionic-angular";

import {Storage} from '@ionic/storage';
import {Visitor} from "../models/visitor";

@Injectable()
export class ApiService {
  private baseUrl: string = 'https://leihkamm.net/slim/enji/public/';
  private meetings;
  private me: User = new NewUser;
  private isReady : Subject<boolean> = new Subject<boolean>();
  private headers;

  constructor(private http: Http,
              public platform: Platform,
              public storage: Storage) {

    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.isReady.next(false);
  }

  // MISC

  public ready(): Observable<boolean> {
    return this.isReady.asObservable();
  }

  public getMe(): User {
    return this.me;
  }

  // USER

  public initUser(): void {
    this.storage.get("user_id").then(success => {
      if (success === null) {
        this.me.user_id = Device.device.uuid === undefined ? String(Date.now()) : Device.device.uuid;
        this.storage.set("user_id", this.me.user_id);
        this.putId().subscribe(success => {
          this.putStatus("Servus! - ein Enjinutzer.");
        });
      } else {
        this.me.user_id = success;
        this.getStatus(this.me.user_id).subscribe(success => this.me.status = success);
      }
      this.isReady.next(true);
    });
  }
  public putId(): Observable<User> {
    let userString = JSON.stringify(this.me);
    return this.http.put(this.baseUrl + "user/id", userString, {headers: this.headers}).map(
      (res: Response) => res.json()
    ).catch(
      (error: any) => Observable.throw(error.json().error || 'Server error')
    );
  }

  public putLocation(lat: number, long: number): Observable<User> {
    this.me.latitude = lat;
    this.me.longitude = long;
    let userString = JSON.stringify(this.me);
    return this.http.put(this.baseUrl + "user/location", userString, {headers: this.headers}).map(
      (res: Response) => res.json()
    ).catch(
      (error: any) => Observable.throw(error.json().error || 'Server error')
    );
  }

  public putStatus(status: string): Observable<User> {
    if (status == "") return;
    this.me.status = status;
    let userString = JSON.stringify(this.me);
    return this.http.put(this.baseUrl + "user/status", userString, {headers: this.headers}).map(
      (res: Response) => res.json()
    ).catch(
      (error: any) => Observable.throw(error.json().error || 'Server error')
    );
  }

  public getStatus(user_id: string): Observable<string> {
    return this.http.get(this.baseUrl + "user/status/" + user_id)
      .map((res: Response) => res.json().status)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .publishReplay(1)
      .refCount();
  }

  public getVisiting(): Observable<Meeting[]> {
    return this.http.get(this.baseUrl + "user/meetings/" + this.me.user_id)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .publishReplay(1)
      .refCount();
  }

  // NEAR

  public getNear(): Observable<User[]> {
    let userString = JSON.stringify(this.me);
    this.http.put(this.baseUrl + "user/location", userString, {headers: this.headers});
    return this.http.get(this.baseUrl + "near/" + this.me.latitude + "/" + this.me.longitude)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .publishReplay(1)
      .refCount();
  }

  // MEETINGS

  public getMeetings(): Observable<Meeting[]> {
    return this.http.get(this.baseUrl + "meetings/" + this.me.latitude + "/" + this.me.longitude)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .publishReplay(1)
      .refCount();
  }

  public getMeeting(event_id: string): any {
    return this.http.get(this.baseUrl + "meeting/" + event_id)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .publishReplay(1)
      .refCount();
  }

  public postMeeting(meeting: Meeting): Observable<any[]> {
    if (this.me.latitude != 0) {
      meeting.longitude = this.me.longitude;
      meeting.latitude = this.me.latitude;
      let meetingString = JSON.stringify(meeting);
      console.log(meeting);
      return this.http.post(this.baseUrl + "meeting", meetingString, {headers: this.headers}).map(
        (res: Response) => res.json()
      ).catch(
        (error: any) => Observable.throw(error.json().error || 'Server error')
      );
    }
  }

  // MESSAGES

  public getMessages(sender_id: string, receiver_id: string): Observable<any> {
    return this.http.get(this.baseUrl + "messages/" + sender_id + "/" + receiver_id)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .publishReplay(1)
      .refCount();
  }


  public getChats(user_id: string): Observable<any> {
    return this.http.get(this.baseUrl + "messages/" + user_id)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .publishReplay(1)
      .refCount();
  }

  public postMessage(message: Message): Observable<any> {
    let messageString = JSON.stringify(message);
    return this.http.post(this.baseUrl + "message", messageString, {headers: this.headers}).map(
      (res: Response) => res.json()
    ).catch(
      (error: any) => Observable.throw(error.json().error || 'Server error')
    );
  }

  // VISITORS

  public postVisitor(visitor: Visitor): Observable<any> {
    let visitorString = JSON.stringify(visitor);
    return this.http.post(this.baseUrl + "visitor", visitorString, {headers: this.headers}).map(
      (res: Response) => res.json()
    ).catch(
      (error: any) => Observable.throw(error.json().error || 'Server error')
    );
  }

  public getVisitors(event_id: string): Observable<any> {
    return this.http.get(this.baseUrl + "visitors/" + event_id)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .publishReplay(1)
      .refCount();
  }

}
