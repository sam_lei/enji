import {Injectable} from '@angular/core';
import {Geolocation} from "ionic-native";
import {Platform} from "ionic-angular";
import {Observable, Subject} from "rxjs";
import {ApiService} from "./api";

@Injectable()
export class GeoService {
  private locationSubject: Subject<boolean> = new Subject<boolean>();
  private locationFinished: boolean = false;

  constructor(public platform: Platform,
              private apiService: ApiService) {

    this.locationSubject.next(false);
  }

  private watchGeoposition(): Observable<any> {
    let options = {
      enableHighAccuracy: false,
      timeout: 8000,
      maximumAge: 10000
    };
    return Geolocation.watchPosition(options);
  }

  public initGeoposition(): void {
    let watcher = this.watchGeoposition()
      .filter((p) => p.code === undefined)
      .subscribe(
        success => {
          this.apiService.putLocation(success.coords.latitude, success.coords.longitude);
          this.locationFound();
        },
        error => {
          this.initGeoposition();
          console.log(error);
        }
      );

      setTimeout(() => {
        if (this.apiService.getMe().latitude == 0) {
          this.locationSubject.next(false);
          watcher.unsubscribe();
          this.initGeoposition();
        } else {
          this.locationFound();
        }
      }, 5000);
  }

  private locationFound() {
    this.locationFinished = true;
    this.locationSubject.next(true);
    this.locationSubject.complete();
  }

  public hasLocation(): Observable<boolean> {
    return this.locationSubject.asObservable();
  }

  public isLocated(): boolean {
    return this.locationFinished;
  }

}
