import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {Http, Response, Jsonp} from "@angular/http";
// import {NewPlacesearch} from "../models/placesearch";
// import {User} from "../models/users";
import {ApiService} from "./api";
/* import {
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapsLatLng,
  CameraPosition,
  GoogleMapsMarkerOptions,
  GoogleMapsMarker
} from 'ionic-native'; */

@Injectable()
export class PlaceService {

  constructor(private http: Http, private apiService: ApiService, private jasonp: Jsonp) {

  }

  public getPlacesAB(keyword: string): Observable<any> {
    let lat = this.apiService.getMe().latitude;
    let long = this.apiService.getMe().longitude;
    let rad = 5000;
    let key = "AIzaSyDpt8yySoQgGZwT4AlTAa1vbqltrSG0CL4";
    let url = encodeURI("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + lat + "," + long + "&radius=" + rad + "&name=" + keyword + "&key=" + key);

    console.log("Placesearch Input:");
    // console.log(placesearch);


    return this.jasonp.get(url)
      .map((res: Response) => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .publishReplay(1)
      .refCount();
  }


  public getPlaces(keyword: string) {
    /*
    var displaySuggestions = function (predictions, status) {
      console.log(predictions);

    };

    var service = new google.maps.places.AutocompleteService();
    service.getQueryPredictions({input: keyword}, displaySuggestions);
  */
  }

}
