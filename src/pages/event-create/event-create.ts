import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {ApiService} from "../../app/services/api";
import {Meeting, NewMeeting} from "../../app/models/meeting";
import {EventsPage} from "../events/events";
import {AdressModal} from "./adress-modal/adress-modal";

@Component({
  selector: 'page-event-create',
  templateUrl: 'event-create.html'
})
export class EventCreatePage {
  public meeting: Meeting;
  public minDate: string;
  public maxYear: number;

  constructor(public navCtrl: NavController,
              private navParams: NavParams,
              private apiService: ApiService) {

    this.meeting = new NewMeeting;
    this.meeting.user_id = this.apiService.getMe().user_id;
    this.minDate = this.meeting.timestamp;
    this.maxYear = new Date().getFullYear() + 1;
  }

  public submitEvent() {
    this.apiService.postMeeting(this.meeting).subscribe(
      data => {
        console.log(data);
      },
      error => {
        console.log(error);
      }
    );
    this.navCtrl.push(EventsPage);
  }

  public swipe(e): void {
    console.log(e);
    if (e.angle < 20 && e.angle > -20 && e.deltaX > 70)
      this.navCtrl.pop();
  }
}
