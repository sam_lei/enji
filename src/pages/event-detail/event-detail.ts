import {Component} from '@angular/core';

import {NavController, NavParams, AlertController, LoadingController} from 'ionic-angular';
import {ApiService} from "../../app/services/api";
import {Meeting} from "../../app/models/meeting";
import {Message, NewMessage} from "../../app/models/message";
import {NewVisitor} from "../../app/models/visitor";
import {NearChatPage} from "../near-chat/near-chat";

@Component({
  selector: 'page-event-detail',
  templateUrl: 'event-detail.html'
})
export class EventDetailPage {
  public meeting: Meeting;
  public my_id: string;
  public event_id: string;
  public loading: any;
  public messages: Message[];
  public isVisitor: boolean = false;

  constructor(public navCtrl: NavController,
              private navParams: NavParams,
              private apiService: ApiService,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController) {


    this.loading = this.loadingCtrl.create({
      content: '',
      spinner: 'crescent',
      showBackdrop: false
    });

    this.event_id = this.navParams.get('id');
    this.my_id = this.apiService.getMe().user_id;
    this.getMeeting();
    this.getVisitors();
    this.getMessages();
  }


  public getMeeting() {
    // this.loading.present();

    this.apiService.getMeeting(this.event_id).subscribe(
      response => {
        this.meeting = response;
      },
      error => {
        console.log(error);
      },
      () => {
        // this.loading.dismiss();
      }
    );
  }

  public subscribe() {
    let visitor = new NewVisitor(this.event_id, this.my_id);
    this.apiService.postVisitor(visitor).subscribe(
      success => {
        this.isVisitor = true;
        this.getVisitors();
      }
    );
  }

  private getVisitors() {
    this.apiService.getVisitors(this.event_id).subscribe(
      response => {
        let me = response.filter(v => v.user_id == this.my_id);
        if (me.length == 1) this.isVisitor = true;
      },
      error => {
        console.log(error);
      }
    );
  }



  public goToChat(): void {
    this.navCtrl.push(
      NearChatPage,
      {
        id: this.meeting.user_id
      }
    );
  }

  public doRefresh(e): void {
    this.apiService.getChats(this.event_id).subscribe(
      response => {
        this.messages = response;
        console.log(response);
        e.complete();
      },
      error => {
        console.log(error);
        e.complete();
      }
    );
  }


  public getMessages() {
    this.apiService.getChats(this.event_id).subscribe(
      response => {
        this.messages = response;
        console.log(response);
      },
      error => {
        console.log(error);
      }
    );
  }

  public newComment(): void {
    let message: Message = new NewMessage;
    message.sender_id = this.my_id;
    message.receiver_id = this.meeting.event_id;

    let prompt = this.alertCtrl.create({
      title: 'Kommentieren',
      inputs: [
        {
          name: 'message',
          placeholder: 'Text'
        }
      ],
      buttons: [
        {
          text: 'Abbrechen'
        },
        {
          text: 'Senden',
          handler: data => {
            message.text = data.message;
            this.apiService.postMessage(message).subscribe(
              response => {
                this.getMessages();
              },
              error => {
                console.log(error);
              });
          }
        }
      ]
    });
    prompt.present();
  }

  public swipe(e): void {
    console.log(e);
    if (e.angle < 20 && e.angle > -20 && e.deltaX > 70)
      this.navCtrl.pop();
  }

}
