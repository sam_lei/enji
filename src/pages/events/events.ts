import {Component} from '@angular/core';

import {NavController, LoadingController, Platform} from 'ionic-angular';
import {Meeting} from "../../app/models/meeting";
import {ApiService} from "../../app/services/api";
import {EventDetailPage} from "../event-detail/event-detail";
import {EventCreatePage} from "../event-create/event-create";
import {PlaceService} from "../../app/services/places";
import {GeoService} from "../../app/services/geo";

@Component({
  selector: 'page-events',
  templateUrl: 'events.html'
})
export class EventsPage {
  public meetings: Meeting[];
  public allMeetings: Meeting[];
  public search: any = "";
  public hasLocation: boolean = true;
  public map: any;

  constructor(public navCtrl: NavController,
              public platform: Platform,
              private apiService: ApiService,
              private geoService: GeoService) {

    this.initEvents();
  }

  public initEvents() {
    this.geoService.hasLocation().subscribe(
      data => {
        this.hasLocation = data;
        if (this.hasLocation)
          this.getMeetings();
      }
    );
    // too late for observer? oldshool!
    if (this.geoService.isLocated())
      this.getMeetings();
  }

  public getMeetings() {
    this.apiService.getMeetings().subscribe(
      response => {
        this.meetings = response;
        this.allMeetings = response;
        console.log(response);
      },
      error => {
        console.log(error);
      }
    );
  }

  public doRefresh(e): void {
    this.apiService.getMeetings().subscribe(
      response => {
        this.meetings = response;
        this.allMeetings = response;
        e.complete();
      },
      error => {
        console.log(error);
        e.complete();
      }
    );
  }

  public goToEventDetail(eventID: number): void {
    console.log("go to:" + eventID);
    this.navCtrl.push(
      EventDetailPage,
      {
        id: eventID
      }
    );
  }

  public goToEventCreate(): void {
    console.log("go to: eventCreate");
    this.navCtrl.push(EventCreatePage);
  }

  public onSearchInput(ev: any) {
    // reset to all meetings
    this.meetings = this.allMeetings;

    // set val to the value of the searchbar
    let val = ev.target.value;
    if (val != undefined && val != "") {
      this.meetings = this.meetings.filter(
        meeting =>
        meeting.title.toLowerCase().indexOf(val.toLowerCase()) > -1
        || meeting.description.toLowerCase().indexOf(val.toLowerCase()) > -1
        || meeting.location_description.toLowerCase().indexOf(val.toLowerCase()) > -1
      );
    }
  }

  public onSearchCancel($event) {
    console.log($event);
  }

}
