import {Component} from '@angular/core';

import {NavController} from 'ionic-angular';
import {ApiService} from "../../app/services/api";

import {Message} from "../../app/models/message";
import {NearChatPage} from "../near-chat/near-chat";
import {Meeting} from "../../app/models/meeting";
import {EventDetailPage} from "../event-detail/event-detail";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public chats: Message[];
  public visiting: Meeting[];
  public me;

  constructor(public navCtrl: NavController,
              public apiService: ApiService) {

    this.initHome();
  }


  private initHome() {
    this.apiService.ready().subscribe(
      data => {
        if (data) {
          this.me = this.apiService.getMe();
          this.updateChats();
          this.updateVisiting();
        }
      }
    );
  }

  public updateVisiting() {
    this.apiService.getVisiting().subscribe(
      success => {
        this.visiting = success;
        console.log(this.visiting);
      },
      error => {
        console.log(error);
      }
    )
  }

  public updateChats() {
    this.apiService.getChats(this.me.user_id).subscribe(
      success => {
        this.filterChats(success);
        console.log(this.chats);
      },
      error => {
        console.log(error);
      }
    )
  }

  public doRefresh(e): void {
    this.apiService.getVisiting().subscribe(
      success => {
        this.visiting = success;
        console.log(this.visiting);
      },
      error => {
        console.log(error);
      }
    );
    this.apiService.getChats(this.me.user_id).subscribe(
      success => {
        this.filterChats(success);
        console.log(this.chats);
        e.complete();
      },
      error => {
        console.log(error);
        e.complete();
      }
    )
  }

  private filterChats(messages: Message[]): void {
    this.chats = [];
    for (let m of messages) {
      if (m.receiver_id.length > 10) {
        let other_id = this.getOtherId(m);
        let duplicates = this.chats.filter(m => m.sender_id == other_id || m.receiver_id == other_id);
        if (duplicates.length == 0) this.chats.push(m);
      } else {

      }
    }
  }

  private getOtherId(m: Message): string {
    if (m.receiver_id == this.me.user_id)
      return m.sender_id;
    else
      return m.receiver_id;
  }


  /* public getMessages(other_id: string): void {
   this.apiService.getMessages(this.apiService.getMe().user_id, other_id).subscribe(
   success => {
   return success;
   }
   )
   } */

  public goToChat(m: Message): void {
    let other_id = this.getOtherId(m);
    console.log("go to:" + other_id);
    this.navCtrl.push(
      NearChatPage,
      {
        id: other_id
      }
    );
  }


  public goToEventDetail(event: Meeting): void {
    console.log("go to:" + event.event_id);
    this.navCtrl.push(
      EventDetailPage,
      {
        id: event.event_id
      }
    );
  }
}
