import {Component, ViewChild} from '@angular/core';

import {NavController, NavParams, AlertController, Content} from 'ionic-angular';
import {ApiService} from "../../app/services/api";
import {Message, NewMessage} from "../../app/models/message";

@Component({
  selector: 'page-near-chat',
  templateUrl: 'near-chat.html'
})
export class NearChatPage {
  @ViewChild(Content) content: Content;
  public my_id: string;
  public other_id: string;
  public other_status: string;
  public loading: any;
  public messages: Message[];
  public newMessage;

  constructor(public navCtrl: NavController,
              private navParams: NavParams,
              private apiService: ApiService,
              public alertCtrl: AlertController) {


    this.my_id = this.apiService.getMe().user_id;
    this.other_id = this.navParams.get('id');
    this.resetMessage();
    this.getMessages();

    this.apiService.getStatus(this.other_id)
      .subscribe(success => this.other_status = success);
  }

  private scrollToBottom() {
    let dimensions = this.content.getContentDimensions();
    this.content.scrollTo(0, dimensions.scrollBottom, 0);
  }

  private resetMessage() {
    this.newMessage = new NewMessage(this.my_id, this.other_id, "");
  }


  public getMessages(): void {
    this.apiService.getMessages(this.my_id, this.other_id).subscribe(
      success => {
        this.messages = success;
        this.scrollToBottom();
      },
      error => {
        this.messages = [{
          sender_id: "",
          receiver_id: "",
          text: "Serverfehler",
          timestamp: 0
        }];
      }
    )
  }

  public sendMessage(): void {
    let prompt = this.alertCtrl.create({
      title: 'Nachricht',
      inputs: [
        {
          name: 'message',
          placeholder: 'Text'
        }
      ],
      buttons: [
        {
          text: 'Abbrechen'
        },
        {
          text: 'Senden',
          handler: data => {
            this.newMessage.text = data.message;
            if (this.newMessage.text != "") {
              this.apiService.postMessage(this.newMessage).subscribe(
                success => {
                  this.resetMessage();
                  this.getMessages();
                },
                error => {
                  console.log(error);
                  this.newMessage.text = "Fehler beim Senden :-(";
                }
              );
            }
          }
        }
      ]
    });
    prompt.present();
  }

  public swipe(e): void {
    console.log(e);
    if (e.angle < 20 && e.angle > -20 && e.deltaX > 70)
      this.navCtrl.pop();
  }

}
