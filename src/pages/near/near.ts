import {Component} from '@angular/core';

import {NavController, AlertController, Platform} from 'ionic-angular';
import {ApiService} from "../../app/services/api";
import {NearChatPage} from "../near-chat/near-chat";
import {User} from "../../app/models/users";
import {GeoService} from "../../app/services/geo";

@Component({
  selector: 'page-near',
  templateUrl: 'near.html'
})
export class NearPage {
  public users: User[] = [];
  public log: string;
  public me: User;
  public hasLocation: boolean = true;

  constructor(public navCtrl: NavController,
              public alertCtrl: AlertController,
              public platform: Platform,
              private apiService: ApiService,
              private geoService: GeoService) {

    this.initNear();
  }

  public initNear() {
    this.me = this.apiService.getMe();
    this.geoService.hasLocation().subscribe(
      data => {
        this.hasLocation = data;
        if (this.hasLocation)
          this.getNear();
      }
    );
    // too late for observer? oldshool!
    if (this.geoService.isLocated())
      this.getNear();
  }

  public getNear() {
    this.apiService.getNear().subscribe(
      success => {
        this.users = success.filter(user => user.user_id != this.me.user_id);
        console.log(success);
      },
      error => {
        console.log(error);
      }
    )
  }


  public doRefresh(e): void {
    this.apiService.getNear().subscribe(
      success => {
        this.users = success.filter(user => user.user_id != this.me.user_id);
        console.log(success);
        e.complete();
      },
      error => {
        console.log(error);
        e.complete();
      }
    )
  }

  public goToNearChat(other_id: string): void {
    console.log("go to:" + other_id);
    this.navCtrl.push(
      NearChatPage,
      {
        id: other_id
      }
    );
  }

  public putStatus() {
    let prompt = this.alertCtrl.create({
      title: "Status",
      message: "",
      inputs: [
        {
          name: "status",
          value: this.me.status,
          placeholder: this.me.status
        },
      ],
      buttons: [
        {
          text: 'Abbrechen',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Speichern',
          handler: data => {
            console.log(data.status);
            this.apiService.putStatus(data.status).subscribe(
              success => {
                this.me = this.apiService.getMe();
                console.log(success);
              }
            );
          }
        }
      ]
    });
    prompt.present();
  }

}
